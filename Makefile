# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/03 11:46:23 by jbailhac          #+#    #+#              #
#    Updated: 2015/03/11 18:17:51 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC			=gcc

CFLAGS		= -Wall -Wextra -Werror -L/usr/X11/lib -lXext -lX11

NAME		=wolf3d

SRC_PATH	=./srcs/

SRC_NAME	=main.c				\
			draw_functions.c	\
			key_hooks.c			\
			raycaster.c			\
			get_stage.c			\
			draw_stage.c		\
			ui.c				\
			spawn_delete_cube.c \
			movements.c

OBJ_PATH	=./objs/

OBJ_NAME	=$(SRC_NAME:.c=.o)

SRC			=$(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ			=$(addprefix $(OBJ_PATH),$(OBJ_NAME))

.PHONY:		all clean fclean re home

all:		$(NAME)

clean:
			@/bin/rm -f $(OBJ)
			@rmdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@echo "\x1b[32mobjects cleaned.\x1b[0m"

fclean:		clean
			@/bin/rm -f $(NAME)
			@echo "\x1b[32m$(NAME) cleaned.\x1b[0m"

$(NAME):	$(OBJ)
			@make -C libft/ >/dev/null
			@make -C minilibx/ >/dev/null
			@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) libft/libft.a minilibx_macos/*.o
			@echo "\x1b[32m$(NAME) done.\x1b[0m"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
			@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@$(CC) -c $(CFLAGS) -o $@ $^
			@echo "$< compiled"

re:			fclean all
