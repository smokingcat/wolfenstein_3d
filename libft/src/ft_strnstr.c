/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 13:31:56 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:30 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	if (!s1 || !s2 || n <= 0)
		return (0);
	if (*s2 == '\0')
		return ((char *)s1);
	if (ft_strlen(s2) > n)
		return (0);
	while (s1[i] && s2[j] && i < n)
	{
		if (s1[i] == s2[j])
			j++;
		else if (s1[i] != s2[j])
		{
			i -= j;
			j = 0;
		}
		i++;
	}
	if (s2[j] == '\0')
		return ((char *)s1 + i - j);
	return (0);
}
