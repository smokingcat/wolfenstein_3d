/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 16:16:22 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:30 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	end;
	size_t	len;

	start = 0;
	if (s == NULL || *s == 0)
		return (ft_strnew(1));
	if (s[0] == '\0')
		return (ft_strnew(1));
	while (start < ft_strlen(s) && \
			(s[start] == ' ' || s[start] == '\n' || s[start] == '\t'))
	{
		start++;
	}
	end = ft_strlen(s) - 1;
	if (end <= 0)
		return (NULL);
	while (end > start && (s[end] == ' ' || s[end] == '\n' || s[end] == '\t'))
	{
		end--;
	}
	len = end - start + 1;
	return (ft_strsub(s, start, len));
}
