/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 12:13:41 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:30 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	char *t_s1;
	char *t_s2;
	char *tmp;

	tmp = (char *)s1;
	if (!s1 || !s2)
		return (0);
	if (!(*s2))
		return ((char *)s1);
	while (*tmp)
	{
		t_s1 = tmp;
		t_s2 = (char *)s2;
		while (*t_s1 && *t_s2 && *t_s1 == *t_s2)
		{
			t_s1++;
			t_s2++;
		}
		if (*t_s2 == '\0')
			return (tmp);
		tmp++;
	}
	return (0);
}
