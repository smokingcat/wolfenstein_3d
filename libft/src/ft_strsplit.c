/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:17:41 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:30 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"
#include <stdlib.h>

static int	count_word(char const *s, char c)
{
	int	wrd_num;
	int i;

	wrd_num = 0;
	i = 0;
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (i != c && s[i])
		{
			i++;
			wrd_num++;
			while (s[i] != c && s[i])
				i++;
		}
	}
	return (wrd_num);
}

char		**ft_strsplit(char const *s, char c)
{
	int		wrd_num;
	int		i;
	int		j;
	int		k;
	char	**tab;

	if (s == NULL)
		return (NULL);
	wrd_num = count_word(s, c);
	tab = (char **)ft_memalloc(sizeof(tab) * wrd_num + 1);
	if (!tab)
		return (NULL);
	i = 0;
	j = -1;
	while (++j <= wrd_num)
	{
		while (s[i] == c)
			i++;
		k = i;
		while (s[i] != c && s[i] != '\0')
			i++;
		tab[j] = ft_strsub(s, k, i - k);
	}
	tab[wrd_num] = NULL;
	return (tab);
}
