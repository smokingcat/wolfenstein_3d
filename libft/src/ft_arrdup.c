/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 13:49:40 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 14:24:51 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

char	**ft_arrdup(char **arr)
{
	char	**copy;
	int		i;

	i = 0;
	while (arr[i])
		i++;
	copy = ft_memalloc(sizeof(char *) * i);
	i = 0;
	while (arr[i])
	{
		copy[i] = ft_strdup(arr[i]);
		i++;
	}
	copy[i] = NULL;
	return (copy);
}
