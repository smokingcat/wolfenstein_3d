/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 14:31:19 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:29 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t i;
	size_t len;

	if (s1 == NULL || s2 == NULL)
		return (0);
	i = 0;
	len = ft_strlen(s1);
	if (n == 0)
		return (1);
	if (ft_strlen(s2) != len)
		return (0);
	while (i < len && i < n)
	{
		if (s1[i] != s2[i])
			return (0);
		i++;
	}
	return (1);
}
