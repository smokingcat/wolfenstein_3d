/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_color_fd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 12:26:29 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:55:59 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

void	ft_putstr_color_fd(char const *s, int fd, int color)
{
	char	*ansi_sequ;

	ansi_sequ = ft_strnew(10);
	ansi_sequ = ft_strcat(ansi_sequ, "\x1b[");
	ft_putstr(ansi_sequ);
	ft_putnbr(color);
	ft_putstr("m");
	ft_putstr_fd(s, fd);
	ft_putstr("\x1b[0m");
	ft_strdel(&ansi_sequ);
}
