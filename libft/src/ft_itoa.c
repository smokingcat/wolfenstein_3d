/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 11:01:21 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:08:50 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"
#include <stdlib.h>

static int			nbrsize(unsigned int nb)
{
	int	i;

	i = 0;
	while (nb > 0)
	{
		i++;
		nb = nb / 10;
	}
	return (i);
}

static unsigned int	val_abs(int nb)
{
	unsigned int abs_n;

	if (nb == -2147483648)
		abs_n = 2147483648;
	if (nb < 0)
		nb = -nb;
	abs_n = (unsigned int)nb;
	return (abs_n);
}

char				*ft_itoa(int n)
{
	char			*str;
	int				i;
	unsigned int	abs_n;

	abs_n = val_abs(n);
	i = nbrsize(abs_n);
	if (n < 0)
		i++;
	str = (char *)malloc(sizeof(*str) * (i + 1));
	if (!str)
		return (NULL);
	if (abs_n == 0)
		str[i++] = '0';
	str[i] = '\0';
	while (i)
	{
		i--;
		str[i] = ((abs_n % 10) + '0');
		abs_n = abs_n / 10;
	}
	if (n < 0)
		str[0] = '-';
	return (str);
}
