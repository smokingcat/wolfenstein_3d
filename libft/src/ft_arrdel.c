/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 13:51:31 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 14:28:50 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"
#include <stdlib.h>

char	**ft_arrdel(char **arr)
{
	int i;
	int len;

	i = 0;
	if (arr == NULL)
		return (NULL);
	len = ft_arrlen(arr);
	while (i < len)
	{
		ft_putchar(i + '0');
		ft_putchar('\n');
		ft_strdel(&arr[i]);
		arr[i] = NULL;
		i++;
	}
	free(arr);
	arr = NULL;
	return (arr);
}
