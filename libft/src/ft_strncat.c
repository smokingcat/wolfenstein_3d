/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 10:47:45 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/06 12:13:29 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/libft.h"

char		*ft_strncat(char *s1, const char *s2, size_t n)
{
	int		s1_len;
	size_t	i;

	s1_len = ft_strlen(s1);
	i = 0;
	while (i < n)
	{
		s1[s1_len] = s2[i];
		s1_len++;
		i++;
	}
	s1[s1_len] = '\0';
	return (s1);
}
