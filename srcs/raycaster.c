/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycaster.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:38:22 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:38:56 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

t_cam	get_cam_dir(t_cam cam)
{
	if (cam.ray_dir_x < 0)
	{
		cam.step_x = -1;
		cam.side_x = (cam.ray_x - cam.map_x) * cam.delta_x;
	}
	else
	{
		cam.step_x = 1;
		cam.side_x = (cam.map_x + 1.0 - cam.ray_x) * cam.delta_x;
	}
	if (cam.ray_dir_y < 0)
	{
		cam.step_y = -1;
		cam.side_y = (cam.ray_y - cam.map_y) * cam.delta_y;
	}
	else
	{
		cam.step_y = 1;
		cam.side_y = (cam.map_y + 1 - cam.ray_y) * cam.delta_y;
	}
	cam.side = 0;
	return (cam);
}

t_cam	get_cam_info(t_env e, int x)
{
	t_cam	cam;

	cam.wall_x = 100;
	cam.p_x = 2 * x / (double)WIDTH - 1;
	cam.ray_x = e.game.p_x;
	cam.ray_y = e.game.p_y;
	cam.ray_dir_x = e.game.dir_x + e.game.plane_x * cam.p_x;
	cam.ray_dir_y = e.game.dir_y + e.game.plane_y * cam.p_x;
	cam.map_x = (int)cam.ray_x;
	cam.map_y = (int)cam.ray_y;
	cam.delta_x = sqrt(1 + (cam.ray_dir_y * cam.ray_dir_y) / \
	(cam.ray_dir_x * cam.ray_dir_x));
	cam.delta_y = sqrt(1 + (cam.ray_dir_x * cam.ray_dir_x) / \
	(cam.ray_dir_y * cam.ray_dir_y));
	cam = get_cam_dir(cam);
	return (cam);
}
