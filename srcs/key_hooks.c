/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hooks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:40:53 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 18:16:42 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

int		movement_hook(int keycode, t_env *e)
{
	if (keycode == 113 || keycode == 65361)
		mv_left(e);
	else if (keycode == 101 || keycode == 65363)
		mv_right(e);
	else if (keycode == 65362)
		mv_forward(e);
	else if (keycode == 65364)
		mv_backward(e);
	draw(e);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == K_ESC || keycode == 65307)
		exit(0);
	else if (keycode == 120)
		delete_cube(e);
	else if (keycode == 112)
		spawn_cube(e);
	else if (keycode == 32 &&\
	e->game.map[(int)e->cube_x][(int)e->cube_y] != LOCKED)
		e->game.map[(int)e->cube_x][(int)e->cube_y] = 0x8F9F77;
	draw(e);
	return (0);
}
