/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spawn_delete_cube.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 18:15:17 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 18:15:18 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void	spawn_cube(t_env *e)
{
	if (e->cube_side == 0 && ((int)e->cube_x + 1 \
	!= (int)e->game.p_x || (int)e->cube_y != (int)e->game.p_y))
		e->game.map[(int)e->cube_x + 1][(int)e->cube_y] = 0x8F1007;
	if (e->cube_side == 1 && ((int)e->cube_x \
	!= (int)e->game.p_x || (int)e->cube_y + 1 != (int)e->game.p_y))
		e->game.map[(int)e->cube_x][(int)e->cube_y + 1] = 0x8F1007;
	if (e->cube_side == 2 && ((int)e->cube_x \
	!= (int)e->game.p_x || (int)e->cube_y - 1 != (int)e->game.p_y))
		e->game.map[(int)e->cube_x][(int)e->cube_y - 1] = 0x8F1007;
	if (e->cube_side == 3 && ((int)e->cube_x - 1 \
	!= (int)e->game.p_x || (int)e->cube_y != (int)e->game.p_y))
		e->game.map[(int)e->cube_x - 1][(int)e->cube_y] = 0x8F1007;
}

void	delete_cube(t_env *e)
{
	if (e->game.map[(int)e->cube_x][(int)e->cube_y] != LOCKED)
		e->game.map[(int)e->cube_x][(int)e->cube_y] = 0;
}
