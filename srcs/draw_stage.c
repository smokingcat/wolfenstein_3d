/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_stage.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:34:04 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:35:22 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void		draw_col(t_env *e, int x, int size, int color)
{
	int y;
	int keep_size;
	int scale;

	scale = size / 1000;
	keep_size = size;
	if (size >= HEIGHT)
		size = HEIGHT - 1;
	else if (size < 0)
		return ;
	y = HEIGHT / 2 - size / 2;
	while (y <= HEIGHT / 2 + size / 2)
	{
		if (y >= HEIGHT / 2 - keep_size / 2 \
				&& y <= HEIGHT / 2 - keep_size / 2 + scale)
			pixel_put_to_image(e, x, y, 0x268bd2);
		else if (y <= HEIGHT / 2 + keep_size / 2 \
				&& y >= HEIGHT / 2 + keep_size / 2 - scale)
			pixel_put_to_image(e, x, y, 0x268bd2);
		else
			pixel_put_to_image(e, x, y, color);
		y++;
	}
}

t_cam		get_line(t_cam cam)
{
	if (cam.side == 0 || cam.side == 3)
	{
		cam.distance = \
		fabs((cam.map_x - cam.ray_x + (1 - cam.step_x) / 2) / cam.ray_dir_x);
		cam.wall_x = cam.ray_y + ((cam.map_x - cam.ray_x + (1.0 - cam.step_x)\
		/ 2.0) / cam.ray_dir_x) * cam.ray_dir_y;
	}
	else
	{
		cam.distance = \
		fabs((cam.map_y - cam.ray_y + (1 - cam.step_y) / 2) / cam.ray_dir_y);
		cam.wall_x = cam.ray_x + ((cam.map_y - cam.ray_y + (1.0 - cam.step_y)\
		/ 2.0) / cam.ray_dir_y) * cam.ray_dir_x;
	}
	cam.line = abs(HEIGHT / cam.distance);
	cam.wall_x -= floor((cam.wall_x));
	return (cam);
}

t_cam		find_if_wall(t_env e, t_cam cam)
{
	cam.wall_type = 0;
	while (cam.wall_type == 0)
	{
		if (cam.side_x < cam.side_y)
		{
			cam.side_x += cam.delta_x;
			cam.map_x += cam.step_x;
			cam.side = 0;
			if (cam.step_x > 0)
				cam.side = 3;
		}
		else
		{
			cam.side_y += cam.delta_y;
			cam.map_y += cam.step_y;
			cam.side = 1;
			if (cam.step_y > 0)
				cam.side = 2;
		}
		cam.wall_type = e.game.map[cam.map_x][cam.map_y];
	}
	return (cam);
}

void		draw_wall(t_env *e, int x, t_cam cam)
{
	draw_col(e, x, cam.line, ((0x00000) + (cam.side * 0.5 * 0x1E1E1E)));
}

void		draw_stage(t_env *e)
{
	int		x;
	t_cam	cam;

	x = 0;
	while (x < WIDTH)
	{
		cam = get_cam_info(*e, x);
		cam = find_if_wall(*e, cam);
		cam = get_line(cam);
		if (x == WIDTH / 2)
		{
			e->cube_x = cam.map_x;
			e->cube_y = cam.map_y;
			e->cube_side = cam.side;
		}
		draw_wall(e, x, cam);
		x++;
	}
}
