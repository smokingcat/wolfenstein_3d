/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:39:35 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:40:33 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void	draw(t_env *e)
{
	load_image(e);
	draw_ui(e);
	draw_minimap(e);
}

t_env	*init_env(void)
{
	t_env *e;

	e = malloc(sizeof(*e) * 1);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WIDTH, HEIGHT, "wolf3d");
	e->img.img_mlx = NULL;
	return (e);
}

int		main(void)
{
	t_env *e;

	e = init_env();
	init_game(e);
	draw(e);
	mlx_key_hook((void *)e->win, key_hook, (void *)e);
	mlx_do_key_autorepeaton(e->mlx);
	mlx_hook(e->win, 2, (1L << 0), movement_hook, e);
	mlx_do_sync(e->mlx);
	mlx_loop(e->mlx);
	return (0);
}
