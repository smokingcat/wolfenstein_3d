/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_stage.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:35:50 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:36:23 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void	init_game(t_env *e)
{
	int i;
	int j;

	i = 0;
	j = 0;
	e->game.map = (int **)malloc(sizeof(int*) * SIZE);
	while (i < SIZE)
	{
		e->game.map[i] = (int *)malloc(sizeof(int) * SIZE);
		j = -1;
		while (++j < SIZE)
		{
			if (j == 0 || j == SIZE - 1 || i == 0 || i == SIZE - 1)
				e->game.map[i][j] = LOCKED;
			else
				e->game.map[i][j] = 0;
		}
		i++;
	}
	e->game.p_x = 5;
	e->game.p_y = 5;
	e->game.dir_x = -1;
	e->game.dir_y = 0;
	e->game.plane_x = 0.0;
	e->game.plane_y = 0.60;
}
