/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movements.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 18:16:25 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 18:18:35 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void	mv_backward(t_env *e)
{
	if (e->game.map\
	[(int)(e->game.p_x - e->game.dir_x * 0.2)][(int)e->game.p_y] == 0)
		e->game.p_x += e->game.dir_x * -0.2;
	if (e->game.map\
	[(int)(e->game.p_x)][(int)(e->game.p_y - e->game.dir_y * 0.2)] == 0)
		e->game.p_y += e->game.dir_y * -0.2;
}

void	mv_forward(t_env *e)
{
	mlx_do_key_autorepeaton(e->mlx);
	if (e->game.map\
			[(int)(e->game.p_x + e->game.dir_x * 0.2)][(int)e->game.p_y] == 0)
		e->game.p_x += e->game.dir_x * 0.2;
	if (e->game.map\
			[(int)e->game.p_x][(int)(e->game.p_y + e->game.dir_y * 0.2)] == 0)
		e->game.p_y += e->game.dir_y * 0.2;
}

void	mv_left(t_env *e)
{
	double old_dir_x;
	double old_plane_x;

	mlx_do_key_autorepeaton(e->mlx);
	old_dir_x = e->game.dir_x;
	e->game.dir_x = e->game.dir_x * cos(0.12) - e->game.dir_y * sin(0.12);
	e->game.dir_y = old_dir_x * sin(0.12) + e->game.dir_y * cos(0.12);
	old_plane_x = e->game.plane_x;
	e->game.plane_x = e->game.plane_x * cos(0.12) - e->game.plane_y * sin(0.12);
	e->game.plane_y = old_plane_x * sin(0.12) + e->game.plane_y * cos(0.12);
}

void	mv_right(t_env *e)
{
	double old_dir_x;
	double old_plane_x;

	mlx_do_key_autorepeaton(e->mlx);
	old_dir_x = e->game.dir_x;
	e->game.dir_x = e->game.dir_x * cos(-0.12) - e->game.dir_y * sin(-0.12);
	e->game.dir_y = old_dir_x * sin(-0.12) + e->game.dir_y * cos(-0.12);
	old_plane_x = e->game.plane_x;
	e->game.plane_x = \
	e->game.plane_x * cos(-0.12) - e->game.plane_y * sin(-0.12);
	e->game.plane_y = old_plane_x * sin(-0.12) + e->game.plane_y * cos(-0.12);
}
