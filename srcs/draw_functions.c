/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:22:42 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:26:23 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

int		pixel_put_to_image(t_env *e, int x, int y, unsigned int color)
{
	unsigned char r;
	unsigned char g;
	unsigned char b;

	r = (color & 0xFF0000) >> 16;
	g = (color & 0xFF00) >> 8;
	b = (0xFF & color);
	e->img.img_data[y * e->img.size_line + x * e->img.bpp / 8] = b;
	e->img.img_data[y * e->img.size_line + x * e->img.bpp / 8 + 1] = g;
	e->img.img_data[y * e->img.size_line + x * e->img.bpp / 8 + 2] = r;
	return (color);
}

void	draw_floor(t_env *e)
{
	int x;
	int y;

	x = -1;
	while (++x < WIDTH)
	{
		y = -1;
		while (++y < HEIGHT / 2)
			pixel_put_to_image(e, x, y, 0x002b36);
		while (++y < HEIGHT)
			pixel_put_to_image(e, x, y, 0x073642);
	}
}

void	load_image(t_env *e)
{
	if (e->img.img_mlx != NULL)
		mlx_destroy_image (e->mlx, e->img.img_mlx);
	e->img.img_mlx = mlx_new_image(e->mlx, WIDTH, HEIGHT);
	e->img.img_data = mlx_get_data_addr(e->img.img_mlx, &(e->img.bpp),\
	&(e->img.size_line), &(e->img.endian));
	draw_floor(e);
	draw_stage(e);
	if (SIZE == 15)
		draw_minimap(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img.img_mlx, 0, 0);
}
