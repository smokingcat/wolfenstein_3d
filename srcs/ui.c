/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 17:23:20 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 17:33:03 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf.h"

void	draw_minisquare(t_env *e, int x, int y, int color)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (++i < 10)
	{
		while (++j < 10)
			if (j == 1 || j == 9 || i == 1 || i == 9)
				pixel_put_to_image(e, x + j, y + i, color);
		j = 0;
	}
}

void	loop_minimap(t_env *e, int x, int y)
{
	int pos_x;
	int pos_y;

	pos_x = 0;
	pos_y = 0;
	while (y < HEIGHT)
	{
		while (x < WIDTH)
		{
			if (e->game.map[pos_x][pos_y] == LOCKED)
				;
			else if (pos_x == (int)e->game.p_x && pos_y == (int)e->game.p_y)
				draw_minisquare(e, x, y, 0xd21f57);
			else if (e->game.map[pos_x][pos_y] != 0)
				draw_minisquare(e, x, y, 0x268bd2);
			x += 10;
			pos_x++;
		}
		x = WIDTH - 150;
		y += 10;
		pos_x = 0;
		pos_y++;
	}
}

void	draw_minimap(t_env *e)
{
	int x;
	int y;

	x = WIDTH - 150;
	y = HEIGHT - 150;
	loop_minimap(e, x, y);
}

void	draw_ui(t_env *e)
{
	int x;
	int y;

	x = (WIDTH / 2);
	y = (HEIGHT / 2);
	mlx_string_put(e->mlx, e->win, x, y, 0xb58900, "+");
	mlx_string_put(e->mlx, e->win, 20, 400, 0xb58900, "        CONTROLS");
	mlx_string_put(e->mlx, e->win, 20, 416, 0xb58900, "----------------------");
	mlx_string_put(e->mlx, e->win, 20, 432, 0xb58900, "[arrows]          move");
	mlx_string_put(e->mlx, e->win, 20, 464, 0xb58900, "[ESC]      get a  life");
	mlx_string_put(e->mlx, e->win, 20, 480, 0xb58900, "[P]        spawn  cube");
	mlx_string_put(e->mlx, e->win, 20, 496, 0xb58900, "[X]        delete cube");
}
