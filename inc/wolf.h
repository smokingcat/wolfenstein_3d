/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 18:19:08 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/11 18:22:41 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include "../minilibx/mlx.h"
# include "../libft/inc/libft.h"
# include <math.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>

# define SIZE 15
# define LOCKED 1
# define WIDTH 800
# define HEIGHT 500
# define K_ESC 53

typedef struct		s_img
{
	void			*img_mlx;
	char			*img_data;
	int				bpp;
	int				size_line;
	int				endian;
}					t_img;

typedef struct		s_game
{
	int				**map;
	double			p_x;
	double			p_y;
	double			dir_x;
	double			dir_y;
	double			plane_x;
	double			plane_y;
	double			time;
	double			old_time;
}					t_game;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	t_img			img;
	t_game			game;
	int				cube_x;
	int				cube_y;
	int				cube_side;
}					t_env;

typedef struct		s_cam
{
	double			p_x;
	double			ray_x;
	double			ray_y;
	double			ray_dir_x;
	double			ray_dir_y;
	double			side_x;
	double			side_y;
	double			delta_x;
	double			delta_y;
	int				map_x;
	int				map_y;
	int				step_x;
	int				step_y;
	int				side;
	double			distance;
	double			wall_x;
	int				line;
	int				wall_type;
}					t_cam;

/*
** UI
*/
void				draw_ui(t_env *e);
void				draw_minimap(t_env *e);

/*
** SPAWN / DELETE CUBES
*/
void				spawn_cube(t_env *e);
void				delete_cube(t_env *e);

/*
** SPAWN / DELETE CUBES
*/
void				mv_backward(t_env *e);
void				mv_forward(t_env *e);
void				mv_left(t_env *e);
void				mv_right(t_env *e);

void				draw(t_env *e);
int					pixel_put_to_image\
					(t_env *e, int x, int y, unsigned int color);
void				load_image(t_env *e);
int					movement_hook(int keycode, t_env *e);
int					key_hook(int keycode, t_env *e);
void				init_game(t_env *e);
void				draw_stage(t_env *e);
t_cam				get_cam_info(t_env e, int x);

#endif
