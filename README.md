# Wolfenstein 3D

"Wolf3d" is a computer graphics experiment written in C on top of minilibx X11 library.

It uses a computer graphics technique called raycasting to simulate a 3D rendering.

## Features

Move around with arrow keys

Add and destroy walls

And a minimalistic minimap

## What we learned

Again, we took advantage of git to coordinate our work on the same project seamlessly

This project will eventually lead us to more complex computer graphics algorithms



![Alt text](http://i.imgur.com/eFOrYCw.png)